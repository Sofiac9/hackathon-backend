package com.example.hackathon.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Aggregate {

    private Double bookCost;
    private Integer totalNumOfStocks;
}
