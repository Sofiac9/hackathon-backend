package com.example.hackathon.service;

import com.example.hackathon.model.Aggregate;
import com.example.hackathon.model.Investment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface Service {

    public List<Investment> findAll();

    public Optional<Investment> findById(String id);

    public ArrayList<Optional<Investment>> findAllUniqueByStockTicker(String stockTicker);

    public Optional<Investment> findByIndustry(String industry);

    public Investment addInvestment(Investment investment);

    public List<Aggregate> aggregate();

    public Investment update(String id, Investment investment);

   /* public Optional<Investment> deleteByStockTicker(String stockTicker);*/

    public Optional<Investment> deleteById(String id);
}
