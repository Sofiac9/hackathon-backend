package com.example.hackathon.repository;

import com.example.hackathon.model.Investment;
import com.example.hackathon.model.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends MongoRepository<Stock, String> {

    Optional<Investment> findByUniqueStockTicker(String stockTicker);

}
