package com.example.hackathon.service;

import com.example.hackathon.model.Aggregate;
import com.example.hackathon.model.Investment;
import com.example.hackathon.model.Stock;
import com.example.hackathon.repository.InvestmentRepository;
import com.example.hackathon.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@org.springframework.stereotype.Service
public class InvestmentService implements Service {

    @Autowired
    InvestmentRepository investmentRepository;

    @Autowired
    StockRepository stockRepository;

    @Override
    public List<Investment> findAll() {
        return investmentRepository.findAll();
    }

    @Override
    public Optional<Investment> findById(String id) {
        return investmentRepository.findById(id);
    }

    @Override
    public Optional<Investment> findByIndustry(String industry) {
        return investmentRepository.findInvestmentByIndustry(industry);
    }

    @Override
    public ArrayList<Optional<Investment>> findAllUniqueByStockTicker(String stockTicker){
        return investmentRepository.findDistinctByStockTicker(stockTicker);
    }

    @Override
    public Investment addInvestment(Investment investment) {
        Stock stock = new Stock();

        if(!stockRepository.findByUniqueStockTicker(investment.getStockTicker()).isPresent()){
            stock.setUniqueStockTicker(investment.getStockTicker());
            stockRepository.save(stock); //not sure how to save the objects and the other fields
        }
        return investmentRepository.save(investment);
    }

    @Override
    public List<Aggregate> aggregate(){
        Double bookCost = 0.0;
        Integer totalNumOfStocks = 0;
        Aggregate aggregate;
        List<Aggregate> aggregates = new ArrayList<Aggregate>();
        for (Stock i : stockRepository.findAll()) {
            List<Optional<Investment>> investments = investmentRepository.findDistinctByStockTicker(i.getUniqueStockTicker());
            aggregate = new Aggregate();
            for (Optional<Investment> j : investments){
                bookCost += (j.get().getPrice() * j.get().getNumOfShares());
                totalNumOfStocks += j.get().getNumOfShares();
            }
            aggregate.setBookCost(bookCost);
            aggregate.setTotalNumOfStocks(totalNumOfStocks);
            aggregates.add(aggregate);

            bookCost = 0.0;
            totalNumOfStocks = 0;
        }
        return aggregates;
    }

    @Override
    public Investment update(String id, Investment investment) {
        Investment investment1 = investmentRepository.findInvestmentById(id);
        if(Objects.nonNull(investment1)){
            if(Objects.nonNull(investment.getPrice())){
                investment1.setPrice(investment.getPrice());
            }
            if(Objects.nonNull(investment.getNumOfShares())){
                investment1.setNumOfShares(investment.getNumOfShares());
            }
        }
        investmentRepository.save(investment1);
        return investment1;
    }

    @Override
    public Optional<Investment> deleteById(String id) {
        return investmentRepository.deleteInvestmentById(id);
    }
}