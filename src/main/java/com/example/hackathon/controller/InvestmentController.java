package com.example.hackathon.controller;

import com.example.hackathon.model.Aggregate;
import com.example.hackathon.model.Investment;
import com.example.hackathon.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/investments")
public class InvestmentController {

    @Autowired
    private Service investmentService;

    @GetMapping()
    public List<Investment> findAll(){
        return investmentService.findAll();
    }

    @GetMapping("id/{id}")
    public ResponseEntity<Investment> findById(@PathVariable String id){
        try {
            return new ResponseEntity<Investment>(investmentService.findById(id).get(), HttpStatus.OK);

        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("industry/{industry}")
    public ResponseEntity<Investment> findByIndustry(@PathVariable String industry) {
        try {
            return new ResponseEntity<Investment>(investmentService.findByIndustry(industry).get(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("aggregate")
    public ResponseEntity<List<Aggregate>> findAllUniqueStockTickers(){
        try {
            return new ResponseEntity<List<Aggregate>>(investmentService.aggregate(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("uniqueStockTicker/{uniqueStockTicker}")
    public ResponseEntity<List<Optional<Investment>>> findAllUniqueStockTickers(@PathVariable String uniqueStockTicker){
        try {
            return new ResponseEntity<List<Optional<Investment>>>(investmentService.findAllUniqueByStockTicker(uniqueStockTicker), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Investment addInvestment( @RequestBody Investment investment ) {
        return investmentService.addInvestment(investment);
    }

    @DeleteMapping("id/{id}")
    public ResponseEntity deleteById(@PathVariable String id){

        if(investmentService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        investmentService.deleteById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("update/{id}")
    public ResponseEntity updateInvestments(@PathVariable String id,@RequestBody Investment investment){
        if(investmentService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Investment>(investmentService.update(id, investment), HttpStatus.OK);
    }
}
