package com.example.hackathon.model;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InvestmentTest {
    @Test
    public void testInvestmentGettersandSetters(){

        LocalDateTime localDateTime = LocalDateTime.now();
        Investment testInvestment = new Investment();
        testInvestment.setId("1009");
        testInvestment.setIndustry("Retail");
        testInvestment.setCompanyName("Amazon");
        testInvestment.setStockTicker("AMZN");
        testInvestment.setNumOfShares(100);
        testInvestment.setPrice(95.96);
        testInvestment.setBuyDate(localDateTime);
        testInvestment.setMarketValue(testInvestment.getPrice());



        assertEquals("1009", testInvestment.getId());
        assertEquals("Retail", testInvestment.getIndustry());
        assertEquals("Amazon", testInvestment.getCompanyName());
        assertEquals("AMZN", testInvestment.getStockTicker());
        assertEquals(100, testInvestment.getNumOfShares());
        assertEquals(95.96, testInvestment.getPrice());
        assertEquals(95.96, testInvestment.getMarketValue());
        Assert.notNull(localDateTime, String.valueOf(localDateTime));
    }
}
