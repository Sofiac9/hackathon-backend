package com.example.hackathon.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@Document
public class Investment {

    @Id
    private String id;
    private String companyName;
    private Double marketValue;
    private String industry;
    private String stockTicker;
    private Double price;
    private Integer numOfShares;
    private LocalDateTime buyDate;
}
