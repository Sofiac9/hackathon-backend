package com.example.hackathon.service;

import com.example.hackathon.model.Aggregate;
import com.example.hackathon.model.Investment;
import com.example.hackathon.model.Stock;
import com.example.hackathon.repository.InvestmentRepository;
import com.example.hackathon.repository.StockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@SpringBootTest
public class InvestmentServiceTest {
    @Autowired
    private InvestmentService investmentService;

    @MockBean
    private InvestmentRepository investmentRepository;

    @MockBean
    private StockRepository stockRepository;

    Investment testInvestment = new Investment();
    Investment testInvestment1 = new Investment();
    List<Investment> investments = new ArrayList<Investment>();
    List<Stock> stocks = new ArrayList<Stock>();
    Stock stock = new Stock();
    Aggregate aggregate =new Aggregate();

    @BeforeEach
    public void setup(){
        testInvestment.setId("1111");
        testInvestment.setIndustry("Consumer Electronics");
        testInvestment.setCompanyName("Apple Inc.");
        testInvestment.setStockTicker("APPL");
        testInvestment.setNumOfShares(100);
        testInvestment.setPrice(95.96);
        testInvestment.setMarketValue(testInvestment.getPrice());
        investments.add(testInvestment);

        testInvestment1.setId("1009");
        testInvestment1.setIndustry("Retail");
        testInvestment1.setCompanyName("Amazon");
        testInvestment1.setStockTicker("AMZN");
        testInvestment1.setNumOfShares(70);
        testInvestment1.setPrice(20.023);
        testInvestment1.setMarketValue(testInvestment1.getPrice());

        investments.add(testInvestment1);

        stock.setUniqueStockTicker("APPL");
        aggregate.setTotalNumOfStocks(100);
        aggregate.setBookCost(9596.00);

    }

    @Test
    public void testInvestmentServiceFindAll() {

        when(investmentRepository.findAll()).thenReturn(investments);

        assertEquals(2, investmentService.findAll().size());
        assertEquals(investments.get(0), investmentService.findAll().get(0));
        assertEquals(investments.get(1), investmentService.findAll().get(1));
    }

    @Test
    public void testInvestmentServiceFindById() {

        investments.add(testInvestment1);

        when(investmentRepository.findById("1009")).thenReturn(java.util.Optional.ofNullable(investments.get(1)));
        when(investmentRepository.findById("1111")).thenReturn(java.util.Optional.ofNullable(investments.get(0)));

        assertEquals(investments.get(0), investmentService.findById("1111").get());
        assertEquals(investments.get(1), investmentService.findById("1009").get());
    }

    @Test
    public void testInvestmentServiceFindAllUniqueByStockTicker() {

        ArrayList<Optional<Investment>> invest = new ArrayList<Optional<Investment>>();
        invest.add(Optional.ofNullable(testInvestment));
        testInvestment1.setStockTicker("APPL");
        invest.add(Optional.ofNullable(testInvestment1));

        when(investmentRepository.findDistinctByStockTicker("APPL")).thenReturn(invest);

        assertEquals(invest, investmentService.findAllUniqueByStockTicker("APPL"));
    }

    @Test
    public void testInvestmentServiceFindByIndustry() {

        when(investmentRepository.findInvestmentByIndustry("Consumer Electronics")).thenReturn(java.util.Optional.ofNullable(investments.get(0)));
        when(investmentRepository.findInvestmentByIndustry("Retail")).thenReturn(java.util.Optional.ofNullable(investments.get(1)));

        assertEquals(investments.get(0), investmentService.findByIndustry("Consumer Electronics").get());
        assertEquals(investments.get(1), investmentService.findByIndustry("Retail").get());
    }

    @Test
    public void testInvestmentServiceAddInvestment() throws Exception{
        when(investmentRepository.save(testInvestment)).thenReturn(testInvestment);
        when(stockRepository.save(stock)).thenReturn(stock);
        assertEquals(testInvestment, investmentService.addInvestment(testInvestment));
        assertEquals(stock, stockRepository.save(stock));
    }

    @Test
    public void testInvestmentServiceUpdate() {

        when(investmentRepository.save(new Investment())).thenReturn(new Investment());

        testInvestment1.setNumOfShares(500);
        when(investmentRepository.findInvestmentById("1009")).thenReturn(testInvestment1);
        when(investmentRepository.save(testInvestment1)).thenReturn(testInvestment1);
        assertEquals(testInvestment1, investmentService.update("1009",testInvestment1));

        testInvestment1.setPrice(60.0);
        when(investmentRepository.findInvestmentById("1009")).thenReturn(testInvestment1);
        when(investmentRepository.save(testInvestment1)).thenReturn(testInvestment1);
        assertEquals(testInvestment1, investmentService.update("1009",testInvestment1));
    }

    @Test
    public void testInvestmentServiceDeleteById() {

        when(investmentRepository.deleteInvestmentById("1111")).thenReturn(java.util.Optional.ofNullable(testInvestment));
        assertEquals(testInvestment, investmentService.deleteById("1111").get());
    }
}
