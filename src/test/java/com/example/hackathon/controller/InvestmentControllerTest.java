package com.example.hackathon.controller;

import com.example.hackathon.model.Investment;
import com.example.hackathon.service.InvestmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(InvestmentController.class)
public class InvestmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InvestmentService investmentService;
    private Investment testInvestment1;
    private List<Investment> allInvestment;


    @BeforeEach
    public void createInvestment() {
        allInvestment = new ArrayList<Investment>();
        testInvestment1 = new Investment();
        testInvestment1.setId("1234");
        testInvestment1.setStockTicker("AMZ");
        testInvestment1.setCompanyName("Amazon");
        testInvestment1.setIndustry("Retail");
        allInvestment.add(testInvestment1);
    }

    @Test
    public void testInvestmentControllerFindAll() throws Exception {
        when(investmentService.findAll()).thenReturn(allInvestment);
        this.mockMvc.perform(get("/api/v1/investments"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }

    @Test
    public void testInvestmentControllerFindById() throws Exception {

        when(investmentService.findById("1234")).thenReturn(java.util.Optional.of(testInvestment1));
        when(investmentService.findById("6789")).thenThrow(new NoSuchElementException("6789"));

        this.mockMvc.perform(get("/api/v1/investments/id/1234"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));

        this.mockMvc.perform(get("/api/v1/investments/id/6789"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testInvestmentControllerFindByIndustry() throws Exception {

        when(investmentService.findByIndustry("Retail")).thenReturn(java.util.Optional.of(testInvestment1));
        when(investmentService.findByIndustry("Random")).thenThrow(new NoSuchElementException("Random"));

        this.mockMvc.perform(get("/api/v1/investments/industry/Retail"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"industry\":\"Retail\"")));

        this.mockMvc.perform(get("/api/v1/investments/industry/Random"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testInvestmentControllerAdd() throws Exception {

        Investment testInvestment = new Investment();
        testInvestment.setId("1111");
        when(investmentService.addInvestment(testInvestment)).thenReturn((testInvestment));

        this.mockMvc.perform(post("/api/v1/investments")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": \"1111\"}"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void testInvestmentControllerDeleteById() throws Exception {

        when(investmentService.findById("1234")).thenReturn(java.util.Optional.of(testInvestment1));
        when(investmentService.findById("6789")).thenReturn(java.util.Optional.empty());

        when(investmentService.deleteById("1234")).thenReturn(java.util.Optional.ofNullable(testInvestment1));

        this.mockMvc.perform(delete("/api/v1/investments/id/1234"))
                .andDo(print())
                .andExpect(status().isNoContent());

        this.mockMvc.perform(delete("/api/v1/investments/id/6789"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    @Test
    public void testInvestmentControllerUpdateInvestment() throws Exception {
        testInvestment1.setPrice(100.0);
        when(investmentService.findById("1234")).thenReturn(java.util.Optional.of(testInvestment1));

        when(investmentService.findById("6789")).thenReturn(java.util.Optional.empty());

        this.mockMvc.perform(put("/api/v1/investments/update/1234")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"price\": 100}"))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(put("/api/v1/investments/update/6789")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"price\": 100}"))
                .andDo(print())
                .andExpect(status().isNotFound());


    }
}
