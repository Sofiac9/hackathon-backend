package com.example.hackathon.repository;

import com.example.hackathon.model.Investment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface InvestmentRepository extends MongoRepository<Investment, String> {

    Investment findInvestmentById(String id);

    Optional<Investment> findInvestmentByStockTicker(String stockTicker);

    ArrayList<Optional<Investment>> findDistinctByStockTicker(String stockTicker);

    Optional<Investment> findInvestmentByIndustry(String industry);

    Optional<Investment> deleteInvestmentByStockTicker(String stockTicker);

    Optional<Investment> deleteInvestmentById(String id);
}
