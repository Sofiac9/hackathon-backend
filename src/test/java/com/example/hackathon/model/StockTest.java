package com.example.hackathon.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockTest {
    Stock stock = new Stock();

    @Test
    public void testStockGettersandSetters() {
        stock.setUniqueStockTicker("JPM");
        assertEquals("JPM", stock.getUniqueStockTicker());
    }
}