package com.example.hackathon.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AggregateTest {
    Aggregate aggregate = new Aggregate();

    @Test
    public void testStockGettersandSetters() {
        aggregate.setBookCost(83.927);
        aggregate.setTotalNumOfStocks(7);

        assertEquals(83.927, aggregate.getBookCost());
        assertEquals(7, aggregate.getTotalNumOfStocks());
    }
}
